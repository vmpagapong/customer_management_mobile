package com.vpagapong.customer_management.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.vpagapong.customer_management.R;
import com.vpagapong.customer_management.interfaces.OnRecyclerviewClick;
import com.vpagapong.customer_management.models.Customer;

import java.util.ArrayList;

public class CustomersAdapter extends RecyclerView.Adapter<CustomersAdapter.ViewHolder> {

    private Context context;
    private LayoutInflater layoutInflater;
    private OnRecyclerviewClick onRecyclerviewClick;
    private ArrayList<Customer> customerArrayList;

    public CustomersAdapter(Context context, ArrayList<Customer> customerArrayList){
        this.context = context;
        this.customerArrayList = customerArrayList;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.listrow_customers, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Customer customer = customerArrayList.get(position);

        holder.tvCustomerName.setText(customer.getName());
        holder.tvCustomerPhoneNumber.setText(customer.getPhoneNumber());
    }

    @Override
    public int getItemCount() {
        return customerArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvCustomerName, tvCustomerPhoneNumber;
        private ConstraintLayout constraintLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvCustomerName = itemView.findViewById(R.id.tv_ListrowCustomerName);
            tvCustomerPhoneNumber = itemView.findViewById(R.id.tv_ListrowCustomerNumber);
            constraintLayout = itemView.findViewById(R.id.constraint_ListrowCustomer);

            constraintLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onRecyclerviewClick != null)
                        onRecyclerviewClick.onRecyclerviewItemClick(v, getAdapterPosition(), 0);
                }
            });
        }
    }

    public void setOnRecyclerviewClick(OnRecyclerviewClick onRecyclerviewClick) {
        this.onRecyclerviewClick = onRecyclerviewClick;
    }
}
