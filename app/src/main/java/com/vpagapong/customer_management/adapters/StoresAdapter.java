package com.vpagapong.customer_management.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.vpagapong.customer_management.R;
import com.vpagapong.customer_management.interfaces.OnRecyclerviewClick;
import com.vpagapong.customer_management.models.Store;

import java.util.ArrayList;

public class StoresAdapter extends RecyclerView.Adapter<StoresAdapter.ViewHolder> {

    private Context context;
    private LayoutInflater layoutInflater;
    private OnRecyclerviewClick onRecyclerviewClick;
    private ArrayList<Store> storeArrayList;

    public StoresAdapter(Context context, ArrayList<Store> storeArrayList) {
        this.context = context;
        this.storeArrayList = storeArrayList;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.listrow_stores, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Store store = storeArrayList.get(position);

        holder.tvStoreName.setText(store.getName());
    }

    @Override
    public int getItemCount() {
        return storeArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvStoreName;
        private ConstraintLayout constraintLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvStoreName = itemView.findViewById(R.id.tv_ListrowStoreName);
            constraintLayout = itemView.findViewById(R.id.constraint_ListrowStore);

            constraintLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onRecyclerviewClick != null)
                        onRecyclerviewClick.onRecyclerviewItemClick(v, getAdapterPosition(), 0);
                }
            });
        }
    }

    public void setOnRecyclerviewClick(OnRecyclerviewClick onRecyclerviewClick) {
        this.onRecyclerviewClick = onRecyclerviewClick;
    }
}
