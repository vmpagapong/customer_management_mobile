package com.vpagapong.customer_management.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.vpagapong.customer_management.R;
import com.vpagapong.customer_management.interfaces.OnRecyclerviewClick;
import com.vpagapong.customer_management.models.Category;

import java.util.ArrayList;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder> {

    private Context context;
    private LayoutInflater layoutInflater;
    private OnRecyclerviewClick onRecyclerviewClick;
    private ArrayList<Category> categoryArrayList;

    public CategoriesAdapter(Context context, ArrayList<Category> categoryArrayList) {
        this.context = context;
        this.categoryArrayList = categoryArrayList;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.listrow_categories, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Category category = categoryArrayList.get(position);

        holder.tvCategoryName.setText(category.getName());
    }

    @Override
    public int getItemCount() {
        return categoryArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvCategoryName;
        private ConstraintLayout constraintLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvCategoryName = itemView.findViewById(R.id.tv_ListrowCategoryName);
            constraintLayout = itemView.findViewById(R.id.constraint_ListrowCategory);

            constraintLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onRecyclerviewClick != null)
                        onRecyclerviewClick.onRecyclerviewItemClick(v, getAdapterPosition(), 0);
                }
            });
        }
    }

    public void setOnRecyclerviewClick(OnRecyclerviewClick onRecyclerviewClick) {
        this.onRecyclerviewClick = onRecyclerviewClick;
    }
}
