package com.vpagapong.customer_management;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.textfield.TextInputLayout;
import com.vpagapong.customer_management.adapters.StoresAdapter;
import com.vpagapong.customer_management.interfaces.OnRecyclerviewClick;
import com.vpagapong.customer_management.models.Store;

import java.util.ArrayList;
import java.util.Objects;

import es.dmoral.toasty.Toasty;

public class MainActivity extends AppCompatActivity {

    private Context context;

    private NavigationView navigationView;
    private View headerView;
    private DrawerLayout drawerLayout;
    private Toolbar toolbar;
    private TextView tvStoreName;
    private AppBarConfiguration mAppBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;

        initializeUI();

        checkStoreSession();

        setSupportActionBar(toolbar);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    private void initializeUI(){
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawerLayout = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);

        headerView = navigationView.getHeaderView(0);
        tvStoreName = headerView.findViewById(R.id.tv_NavStoreName);

        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_dashboard, R.id.nav_customers, R.id.nav_profile, R.id.nav_settings,
                R.id.nav_category, R.id.nav_transactions, R.id.nav_payments)
                .setDrawerLayout(drawerLayout)
                .build();
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_stores)
            openStoreSelectionDialog();

        return super.onOptionsItemSelected(item);
    }

    private void openStoreSelectionDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_selection_store, null);
        final RecyclerView recyclerView;

        recyclerView = dialogView.findViewById(R.id.rv_Stores);

        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();

        final ArrayList<Store> storeArrayList = Store.read(context, "");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        StoresAdapter storesAdapter = new StoresAdapter(context, storeArrayList);
        storesAdapter.setOnRecyclerviewClick(new OnRecyclerviewClick() {
            @Override
            public void onRecyclerviewItemClick(View view, int position, int fromButton) {
                Store store = storeArrayList.get(position);
                setSelectedStore(store);
                b.hide();
            }
        });
        recyclerView.setAdapter(storesAdapter);

        b.show();
        Objects.requireNonNull(b.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void setSelectedStore(Store store){
        Store.clearStoreSession(context);

        store.saveStoreSession(context);

        checkStoreSession();
    }

    private void checkStoreSession() {
        if (Store.getID(context) == 0)
            openAddStoreDialog();
        else
            tvStoreName.setText(Store.getName(context));
    }

    private void openAddStoreDialog(){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_add_store, null);
        final TextInputLayout etName;
        final Button btnSave;

        etName = dialogView.findViewById(R.id.et_AddStoreName);
        btnSave = dialogView.findViewById(R.id.btn_AddStoreSave);

        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = etName.getEditText().getText().toString();

                if (name.isEmpty())
                    Toasty.warning(context, "Please input store name.").show();
                else{
                    saveStore(name);
                    b.hide();

                    checkStoreSession();
                }
            }
        });

        b.show();
        b.setCancelable(false);
        Objects.requireNonNull(b.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void saveStore(String name){
        Store store = new Store();
        store.setName(name);

        Pair<Boolean, Integer> result = store.save(context);

        if (result.first){
            store.setId(result.second);
            store.saveStoreSession(context);
            Toasty.success(context, "Success").show();
        }
    }
}
