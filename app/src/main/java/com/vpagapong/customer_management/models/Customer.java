package com.vpagapong.customer_management.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Pair;

import com.vpagapong.customer_management.dbClasses.DatabaseAdapter;
import com.vpagapong.customer_management.utils.Converter;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class Customer {

    private int id;
    private String name;
    private String phoneNumber;
    private String address;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public static ArrayList<Customer> read(Context context, String searchQuery) {
        ArrayList<Customer> customerArrayList = new ArrayList<>();

        try {
            DatabaseAdapter db = new DatabaseAdapter(context);
            String selectQuery;

            if (!searchQuery.isEmpty())
                selectQuery = "SELECT * \n" +
                        "FROM customer \n" +
                        "WHERE name \n" +
                        "LIKE %" + searchQuery + "% \n" +
                        "ORDER BY name \n" +
                        "ASC";
            else
                selectQuery = "SELECT * \n" +
                        "FROM customer \n" +
                        "ORDER BY name \n" +
                        "ASC";

            Cursor cursor = db.open().getDatabaseInstance().rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    Customer customer = new Customer();

                    customer.setId(Converter.ObjectToInt(cursor, "id"));
                    customer.setName(Converter.ObjectToString(cursor, "name"));
                    customer.setPhoneNumber(Converter.ObjectToString(cursor, "phone_number"));
                    customer.setAddress(Converter.ObjectToString(cursor, "address"));

                    customerArrayList.add(customer);

                } while (cursor.moveToNext());
            }
            cursor.close();

            DatabaseAdapter.close();

            return customerArrayList;

        } catch (Exception err) {
            Toasty.error(context, err.toString()).show();
            return customerArrayList;
        }
    }

    public Pair<Boolean, Integer> save(Context context) {
        try {
            DatabaseAdapter db = new DatabaseAdapter(context.getApplicationContext());

            ContentValues contentValues = new ContentValues();

            contentValues.put("name", name);
            contentValues.put("phone_number", phoneNumber);
            contentValues.put("address", address);

            return db.save(contentValues, "customer");
        } catch (SQLException err) {
            Toasty.error(context, err.toString()).show();
            return new Pair<>(false, -1);
        }
    }

    public void update(Context context, int customerId) {
        DatabaseAdapter db = new DatabaseAdapter(context);

        ContentValues contentValues = new ContentValues();

        contentValues.put("name", name);
        contentValues.put("phone_number", phoneNumber);
        contentValues.put("address", address);

        db.update(contentValues, "customer", "id=?", new String[]{String.valueOf(customerId)});
    }

    public void delete(Context context, int customerId) {
        DatabaseAdapter db = new DatabaseAdapter(context);

        db.delete("customer", "id=?", new String[]{String.valueOf(customerId)});
    }
}
