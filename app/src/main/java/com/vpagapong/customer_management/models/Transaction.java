package com.vpagapong.customer_management.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.SQLException;
import android.util.Pair;

import com.vpagapong.customer_management.dbClasses.DatabaseAdapter;
import com.vpagapong.customer_management.utils.DateTimeHandler;

import java.util.Date;

import es.dmoral.toasty.Toasty;

public class Transaction {

    public static final int PAID = 0;
    public static final int UNPAID = 1;

    private int id;
    private Customer customer;
    private Store store;
    private double totalPayment;
    private double totalBalance;
    private Date transactionDate;
    private Date dueDate;
    private int status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public double getTotalPayment() {
        return totalPayment;
    }

    public void setTotalPayment(double totalPayment) {
        this.totalPayment = totalPayment;
    }

    public double getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(double totalBalance) {
        this.totalBalance = totalBalance;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

//    public static ArrayList<Transaction> read(Context context, String searchQuery) {
//        ArrayList<Transaction> transactionArrayList = new ArrayList<>();
//
//        try {
//            DatabaseAdapter db = new DatabaseAdapter(context);
//            String selectQuery;
//
//            if (!searchQuery.isEmpty())
//                selectQuery = "SELECT * FROM " + TblTransaction.TBL_NAME + " WHERE " + TblCustomer.NAME + "=" + searchQuery + " ORDER BY " + TblCustomer.NAME + " ASC";
//            else
//                selectQuery = "SELECT *, customer.name, customer.number, customer.address FROM " + TblTransaction.TBL_NAME + " JOIN " + TblCustomer.TBL_NAME + " ON " + TblCustomer.ID + "=" + TblTransaction.CUSTOMER_ID +" ORDER BY " + TblCustomer.NAME + " ASC";
//
//            Cursor cursor = db.open().getDatabaseInstance().rawQuery(selectQuery, null);
//
//            if (cursor.moveToFirst()) {
//                do {
//                    Transaction transaction = new Transaction();
//                    Customer customer = new Customer();
//                    Store store = new Store();
//
//                    customer.setName(Converter.ObjectToString(cursor, TblCustomer.NAME));
//
//                    transaction.setId(Converter.ObjectToInt(cursor, TblCustomer.ID));
//                    transaction.setCustomer();
//                    transaction.setPhoneNumber(Converter.ObjectToString(cursor, TblCustomer.PHONE_NUMBER));
//                    transaction.setAddress(Converter.ObjectToString(cursor, TblCustomer.ADDRESS));
//
//                    transactionArrayList.add(transaction);
//
//                } while (cursor.moveToNext());
//            }
//            cursor.close();
//
//            DatabaseAdapter.close();
//
//            return transactionArrayList;
//
//        } catch (Exception err) {
//            Toasty.error(context, err.toString()).show();
//            return transactionArrayList;
//        }
//    }

    public Pair<Boolean, Integer> save(Context context) {
        try {
            DatabaseAdapter db = new DatabaseAdapter(context.getApplicationContext());

            ContentValues contentValues = new ContentValues();

            contentValues.put("customer_id", getCustomer().getId());
            contentValues.put("store_id", getStore().getId());
            contentValues.put("total_payment", getTotalPayment());
            contentValues.put("total_balance", getTotalBalance());
            contentValues.put("transaction_date", DateTimeHandler.convertDateToStringDate(getTransactionDate()));
            contentValues.put("due_date", DateTimeHandler.convertDateToStringDate(getDueDate()));
            contentValues.put("status", UNPAID);

            return db.save(contentValues, "transaction");
        } catch (SQLException err) {
            Toasty.error(context, err.toString()).show();
            return new Pair<>(false, -1);
        }
    }
}
