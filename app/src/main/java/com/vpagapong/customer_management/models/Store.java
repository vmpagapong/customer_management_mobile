package com.vpagapong.customer_management.models;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.preference.PreferenceManager;
import android.util.Pair;

import com.vpagapong.customer_management.dbClasses.DatabaseAdapter;
import com.vpagapong.customer_management.utils.Converter;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class Store {

    public int id;
    public String name;

    public Store() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static ArrayList<Store> read(Context context, String searchQuery) {
        ArrayList<Store> storeArrayList = new ArrayList<>();

        try {
            DatabaseAdapter db = new DatabaseAdapter(context);
            String selectQuery;

            if (!searchQuery.isEmpty())
                selectQuery = "SELECT * \n" +
                        "FROM store \n" +
                        "WHERE name \n" +
                        "LIKE %" + searchQuery + "% \n" +
                        "ORDER BY name \n" +
                        "ASC";
            else
                selectQuery = "SELECT * \n" +
                        "FROM store \n" +
                        "ORDER BY name \n" +
                        "ASC";

            Cursor cursor = db.open().getDatabaseInstance().rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    Store store = new Store();

                    store.setId(Converter.ObjectToInt(cursor, "id"));
                    store.setName(Converter.ObjectToString(cursor, "name"));

                    storeArrayList.add(store);

                } while (cursor.moveToNext());
            }
            cursor.close();

            DatabaseAdapter.close();

            return storeArrayList;

        } catch (Exception err) {
            Toasty.error(context, err.toString()).show();
            return storeArrayList;
        }
    }

    public Pair<Boolean, Integer> save(Context context) {
        try {
            DatabaseAdapter db = new DatabaseAdapter(context.getApplicationContext());

            ContentValues contentValues = new ContentValues();

            contentValues.put("name", name);

            return db.save(contentValues, "store");
        } catch (SQLException err) {
            Toasty.error(context, err.toString()).show();
            return new Pair<>(false, -1);
        }
    }

    public void update(Context context, int storeId) {
        DatabaseAdapter db = new DatabaseAdapter(context);

        ContentValues contentValues = new ContentValues();

        contentValues.put("name", name);

        db.update(contentValues, "store", "id=?", new String[]{String.valueOf(storeId)});
    }

    public void delete(Context context, int storeId) {
        DatabaseAdapter db = new DatabaseAdapter(context);

        db.delete("store", "id=?", new String[]{String.valueOf(storeId)});
    }

    public static int getID(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getInt("ID", 0);
    }

    public static String getName(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        return settings.getString("NAME", "");
    }

    public boolean saveStoreSession(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt("ID", id);
        editor.putString("NAME", name);
        return editor.commit();
    }

    public static boolean clearStoreSession(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        return editor.commit();
    }
}
