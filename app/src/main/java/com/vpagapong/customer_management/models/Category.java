package com.vpagapong.customer_management.models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.util.Pair;

import com.vpagapong.customer_management.dbClasses.DatabaseAdapter;
import com.vpagapong.customer_management.utils.Converter;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class Category {

    private int id;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static ArrayList<Category> read(Context context, String searchQuery) {
        ArrayList<Category> categoryArrayList = new ArrayList<>();

        try {
            DatabaseAdapter db = new DatabaseAdapter(context);
            String selectQuery;

            if (!searchQuery.isEmpty())
                selectQuery = "SELECT * \n" +
                        "FROM category \n" +
                        "WHERE name \n" +
                        "LIKE %" + searchQuery + "& \n" +
                        "ORDER BY name \n" +
                        "ASC";
            else
                selectQuery = "SELECT * \n" +
                        "FROM category \n" +
                        "ORDER BY name \n" +
                        "ASC";

            Cursor cursor = db.open().getDatabaseInstance().rawQuery(selectQuery, null);

            if (cursor.moveToFirst()) {
                do {
                    Category category = new Category();

                    category.setId(Converter.ObjectToInt(cursor, "id"));
                    category.setName(Converter.ObjectToString(cursor, "name"));

                    categoryArrayList.add(category);

                } while (cursor.moveToNext());
            }
            cursor.close();

            DatabaseAdapter.close();

            return categoryArrayList;

        } catch (Exception err) {
            Toasty.error(context, err.toString()).show();
            return categoryArrayList;
        }
    }

    public Pair<Boolean, Integer> save(Context context) {
        try {
            DatabaseAdapter db = new DatabaseAdapter(context.getApplicationContext());

            ContentValues contentValues = new ContentValues();

            contentValues.put("name", name);

            return db.save(contentValues, "category");
        } catch (SQLException err) {
            Toasty.error(context, err.toString()).show();
            return new Pair<>(false, -1);
        }
    }

    public void update(Context context, int categoryId) {
        DatabaseAdapter db = new DatabaseAdapter(context);

        ContentValues contentValues = new ContentValues();

        contentValues.put("name", name);

        db.update(contentValues, "category", "id=?", new String[]{String.valueOf(categoryId)});
    }

    public void delete(Context context, int categoryId) {
        DatabaseAdapter db = new DatabaseAdapter(context);

        db.delete("category", "id=?", new String[]{String.valueOf(categoryId)});
    }
}
