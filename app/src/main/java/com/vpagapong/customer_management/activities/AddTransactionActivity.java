package com.vpagapong.customer_management.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;

import com.vpagapong.customer_management.R;

public class AddTransactionActivity extends AppCompatActivity {

    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_transaction);

        getSupportActionBar().setTitle("New Transaction");

        context = this;

        initializeUI();
    }

    private void initializeUI(){

    }
}
