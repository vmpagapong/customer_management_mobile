package com.vpagapong.customer_management.fragments;

import android.content.Context;
import android.content.Intent;
import android.content.ReceiverCallNotAllowedException;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.vpagapong.customer_management.R;
import com.vpagapong.customer_management.activities.AddTransactionActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class TransactionsFragment extends Fragment {

    private Context context;
    private View view;

    private RecyclerView recyclerView;
    private FloatingActionButton floatingActionButton;

    public TransactionsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_transactions, container, false);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        context = getContext();

        initializeUI();
    }

    private void initializeUI(){
        recyclerView = view.findViewById(R.id.rv_Transactions);
        floatingActionButton = view.findViewById(R.id.fab_Transactions);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, AddTransactionActivity.class);
                startActivity(intent);
            }
        });

        getTransactions();
    }

    private void getTransactions(){

    }
}
