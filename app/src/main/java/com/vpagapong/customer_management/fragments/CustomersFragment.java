package com.vpagapong.customer_management.fragments;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import com.vpagapong.customer_management.R;
import com.vpagapong.customer_management.adapters.CustomersAdapter;
import com.vpagapong.customer_management.adapters.StoresAdapter;
import com.vpagapong.customer_management.interfaces.OnRecyclerviewClick;
import com.vpagapong.customer_management.models.Customer;
import com.vpagapong.customer_management.models.Store;

import java.util.ArrayList;
import java.util.Objects;

import es.dmoral.toasty.Toasty;

/**
 * A simple {@link Fragment} subclass.
 */
public class CustomersFragment extends Fragment {

    private Context context;
    private View view;

    private FloatingActionButton floatingActionButton;
    private RecyclerView recyclerView;

    private ArrayList<Customer> customerArrayList = new ArrayList<>();
    private CustomersAdapter customersAdapter;

    public CustomersFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_customers, container, false);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        context = getContext();

        initializeUI();

        getCustomers();
    }

    private void initializeUI() {
        floatingActionButton = view.findViewById(R.id.fab_Customers);
        recyclerView = view.findViewById(R.id.rv_Customers);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddCustomerDialog();
            }
        });
    }

    private void openAddCustomerDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_add_customer, null);
        final TextInputLayout etName, etNumber, etAddress;
        final Button btnSave;

        etName = dialogView.findViewById(R.id.et_AddCustomerName);
        etNumber = dialogView.findViewById(R.id.et_AddCustomerNumber);
        etAddress = dialogView.findViewById(R.id.et_AddCustomerAddress);
        btnSave = dialogView.findViewById(R.id.btn_AddCustomerSave);

        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = etName.getEditText().getText().toString();
                String number = etNumber.getEditText().getText().toString();
                String address = etAddress.getEditText().getText().toString();

                if (name.isEmpty())
                    Toasty.warning(context, "Please input customer name.").show();
                else {
                    if (number.isEmpty())
                        Toasty.warning(context, "Please input customer phone number.").show();
                    else{
                        saveCustomer(name, number, address);
                        getCustomers();
                        b.hide();
                    }
                }
            }
        });

        b.show();
        Objects.requireNonNull(b.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void saveCustomer(String name, String number, String address){
        Customer customer = new Customer();
        customer.setName(name);
        customer.setPhoneNumber(number);
        customer.setAddress(address);

        Pair<Boolean, Integer> result = customer.save(context);
        if (result.first)
            Toasty.success(context, "Success").show();
    }

    private void getCustomers(){
        customerArrayList = Customer.read(context, "");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        CustomersAdapter customersAdapter = new CustomersAdapter(context, customerArrayList);
        customersAdapter.setOnRecyclerviewClick(new OnRecyclerviewClick() {
            @Override
            public void onRecyclerviewItemClick(View view, int position, int fromButton) {
                Customer customer = customerArrayList.get(position);


            }
        });
        recyclerView.setAdapter(customersAdapter);
    }
}
