package com.vpagapong.customer_management.fragments;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import com.vpagapong.customer_management.R;
import com.vpagapong.customer_management.adapters.CategoriesAdapter;
import com.vpagapong.customer_management.adapters.CustomersAdapter;
import com.vpagapong.customer_management.interfaces.OnRecyclerviewClick;
import com.vpagapong.customer_management.models.Category;
import com.vpagapong.customer_management.models.Customer;

import java.util.ArrayList;
import java.util.Objects;

import es.dmoral.toasty.Toasty;

/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryFragment extends Fragment {

    private Context context;
    private View view;

    private RecyclerView recyclerView;
    private FloatingActionButton floatingActionButton;

    private ArrayList<Category> categoryArrayList = new ArrayList<>();

    public CategoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_category, container, false);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

        context = getContext();

        initializeUI();

        getCategories();
    }

    private void initializeUI() {
        recyclerView = view.findViewById(R.id.rv_Categories);
        floatingActionButton = view.findViewById(R.id.fab_Categories);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAddCategoryDialog();
            }
        });
    }

    private void getCategories(){
        categoryArrayList = Category.read(context, "");

        GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 2);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setHasFixedSize(true);
        CategoriesAdapter categoriesAdapter = new CategoriesAdapter(context, categoryArrayList);
        categoriesAdapter.setOnRecyclerviewClick(new OnRecyclerviewClick() {
            @Override
            public void onRecyclerviewItemClick(View view, int position, int fromButton) {
                Category category = categoryArrayList.get(position);


            }
        });
        recyclerView.setAdapter(categoriesAdapter);
    }

    private void openAddCategoryDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);

        LayoutInflater inflater = getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_add_category, null);
        final TextInputLayout etName;
        final Button btnSave;

        etName = dialogView.findViewById(R.id.et_AddCategoryName);
        btnSave = dialogView.findViewById(R.id.btn_AddCategorySave);

        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = etName.getEditText().getText().toString();

                if (name.isEmpty())
                    Toasty.warning(context, "Please input category name.").show();
                else {
                    saveCategory(name);
                    getCategories();
                    b.hide();
                }
            }
        });

        b.show();
        Objects.requireNonNull(b.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    private void saveCategory(String name) {
        Category category = new Category();
        category.setName(name);

        Pair<Boolean, Integer> result = category.save(context);

        if (result.first)
            Toasty.success(context, "Success").show();
    }
}
