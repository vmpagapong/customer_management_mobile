package com.vpagapong.customer_management.dbClasses;

public class TblItem {

    public static final String TBL_NAME = "item";
    public static final String ID = "id";
    public static final String TRANSACTION_ID = "transaction_id";
    public static final String CATEGORY_ID = "category_id";
    public static final String ITEM_NAME = "item_name";
    public static final String ITEM_ORIGINAL_PRICE = "item_original_price";
    public static final String ITEM_ORIGINAL_PRICE_DISCOUNT = "item_original_price_discount";
    public static final String ITEM_ACTUAL_PRICE = "item_actual_price";
    public static final String ITEM_ACTUAL_PRICE_DISCOUNT = "item_actual_price_discount";
    public static final String ITEM_QUANTITY = "item_quantity";
    public static final String TOTAL_ORIGINAL_PRICE = "total_original_price";
    public static final String TOTAL_ACTUAL_PRICE = "total_actual_price";
}
