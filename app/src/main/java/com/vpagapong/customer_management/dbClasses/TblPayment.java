package com.vpagapong.customer_management.dbClasses;

public class TblPayment {

    public static final String TBL_NAME = "payment";
    public static final String ID = "id";
    public static final String TRANSACTION_ID = "transaction_id";
    public static final String AMOUNT = "amount";
    public static final String PAYMENT_DATE = "payment_date";
}
