package com.vpagapong.customer_management.dbClasses;

public class TblCategory {

    public static final String TBL_NAME = "category";
    public static final String ID = "id";
    public static final String NAME = "name";
}
