package com.vpagapong.customer_management.dbClasses;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.vpagapong.customer_management.utils.Debugger;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "db_management";
    private static final int DATABASE_VERSION = 1;

    private String storeTable;
    private String customerTable;
    private String categoryTable;
    private String transactionTable;
    private String itemTable;
    private String paymentTable;

    public DatabaseHelper(@Nullable Context context, @Nullable SQLiteDatabase.CursorFactory factory) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            initializeTables();

            db.execSQL(storeTable);
            db.execSQL(categoryTable);
            db.execSQL(customerTable);
            db.execSQL(transactionTable);
            db.execSQL(itemTable);
            db.execSQL(paymentTable);
        } catch (SQLException err) {
            Debugger.logD(err.toString());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private void initializeTables() {
        storeTable = "CREATE TABLE " + TblStore.TBL_NAME + "(";
        storeTable += TblStore.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,";
        storeTable += TblStore.NAME + " TEXT)";

        categoryTable = "CREATE TABLE " + TblCategory.TBL_NAME + "(";
        categoryTable += TblCategory.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,";
        categoryTable += TblCategory.NAME + " TEXT)";

        customerTable = "CREATE TABLE " + TblCustomer.TBL_NAME + "(";
        customerTable += TblCustomer.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,";
        customerTable += TblCustomer.NAME + " TEXT,";
        customerTable += TblCustomer.PHONE_NUMBER + " TEXT,";
        customerTable += TblCustomer.ADDRESS + " TEXT)";

        transactionTable = "CREATE TABLE " + TblTransaction.TBL_NAME + "(";
        transactionTable += TblTransaction.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,";
        transactionTable += TblTransaction.CUSTOMER_ID + " INTEGER,";
        transactionTable += TblTransaction.STORE_ID + " INTEGER,";
        transactionTable += TblTransaction.TOTAL_PAYMENT + " NUMERIC,";
        transactionTable += TblTransaction.TOTAL_BALANCE + " NUMERIC,";
        transactionTable += TblTransaction.TRANSACTION_DATE + " DATETIME";
        transactionTable += TblTransaction.DUE_DATE + " DATETIME";
        transactionTable += TblTransaction.STATUS + " INTEGER)";

        itemTable = "CREATE TABLE " + TblItem.TBL_NAME + "(";
        itemTable += TblItem.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,";
        itemTable += TblItem.TRANSACTION_ID + " INTEGER,";
        itemTable += TblItem.CATEGORY_ID + " INTEGER,";
        itemTable += TblItem.ITEM_NAME + " TEXT,";
        itemTable += TblItem.ITEM_QUANTITY + " INTEGER,";
        itemTable += TblItem.ITEM_ORIGINAL_PRICE + " NUMERIC,";
        itemTable += TblItem.ITEM_ORIGINAL_PRICE_DISCOUNT + " INTEGER,";
        itemTable += TblItem.ITEM_ACTUAL_PRICE + " NUMERIC,";
        itemTable += TblItem.ITEM_ACTUAL_PRICE_DISCOUNT + " INTEGER,";
        itemTable += TblItem.TOTAL_ACTUAL_PRICE + " NUMERIC,";
        itemTable += TblItem.TOTAL_ORIGINAL_PRICE + " NUMERIC)";

        paymentTable = "CREATE TABLE " + TblPayment.TBL_NAME + "(";
        paymentTable += TblPayment.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,";
        paymentTable += TblPayment.TRANSACTION_ID + " INTEGER,";
        paymentTable += TblPayment.AMOUNT + " NUMERIC,";
        paymentTable += TblPayment.PAYMENT_DATE + " DATETIME)";
    }
}
