package com.vpagapong.customer_management.dbClasses;

public class TblCustomer {

    public static final String TBL_NAME = "customer";
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String PHONE_NUMBER = "phone_number";
    public static final String ADDRESS = "address";
}
