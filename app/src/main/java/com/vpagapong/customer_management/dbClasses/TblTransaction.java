package com.vpagapong.customer_management.dbClasses;

public class TblTransaction {

    public static final String TBL_NAME = "transaction";
    public static final String ID = "id";
    public static final String CUSTOMER_ID = "customer_id";
    public static final String STORE_ID = "store_id";
    public static final String TOTAL_PAYMENT = "total_payment";
    public static final String TOTAL_BALANCE = "total_balance";
    public static final String TRANSACTION_DATE = "transaction_date";
    public static final String DUE_DATE = "due_date";
    public static final String STATUS = "status";
}
