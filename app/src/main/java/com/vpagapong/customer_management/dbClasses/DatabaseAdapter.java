package com.vpagapong.customer_management.dbClasses;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Pair;

import es.dmoral.toasty.Toasty;

public class DatabaseAdapter {

    public static SQLiteDatabase db;
    private final Context context;
    private DatabaseHelper dbHelper;

    public DatabaseAdapter(Context context) {
        this.context = context;
        dbHelper = new DatabaseHelper(context, null);
    }

    public DatabaseAdapter open() throws SQLException {
        db = dbHelper.getWritableDatabase();
        return this;
    }

    public static void dbEndTransaction() {
        if (db.inTransaction())
            db.endTransaction();
    }

    public static void close() {
        dbEndTransaction();
        db.close();
    }

    public SQLiteDatabase getDatabaseInstance() {
        return db;
    }

    public Pair<Boolean, Integer> save(ContentValues contentValues, String table) {
        try {
            open();

            long newId = db.insert(table, null, contentValues);

            if (newId == -1)
                throw new SQLException("INSERT ERROR");

            if (db.inTransaction())
                db.endTransaction();

            close();

            return new Pair<>(true, (int) newId);
        } catch (SQLException err) {
            Toasty.error(context, err.toString()).show();
            return new Pair<>(false, -1);
        }
    }

    public int delete(String table, String where, String[] Args) {
        open();

        int numberOFEntriesDeleted = db.delete(table, where, Args);

        if (db.inTransaction())
            db.endTransaction();

        close();

        return numberOFEntriesDeleted;
    }

    public Cursor read(String query) {
        Cursor cursor = open().getDatabaseInstance().rawQuery(query, null);
        return cursor;
    }

    public int update(ContentValues contentValues, String table, String where, String[] args) {
        open();

        int result = db.update(table, contentValues, where, args);

        close();

        return result;
    }
}
