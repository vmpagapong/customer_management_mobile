package com.vpagapong.customer_management.interfaces;

import android.view.View;

public interface OnRecyclerviewClick {
    void onRecyclerviewItemClick(View view, int position, int fromButton);
}
