package com.vpagapong.customer_management.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateTimeHandler {

    public static String getTimeStamp(){
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd-hh-mm");
            String format = simpleDateFormat.format(new Date());
            return format.replace("-","");

        } catch (Exception err) {
            return "";
        }
    }

    public static Date getDueDate(Date date, int daysInterval){
        try {
            final Calendar c = Calendar.getInstance();
            c.setTime(date);
            c.add(Calendar.DATE, daysInterval);

            return c.getTime();

        } catch (Exception err) {
            return null;
        }
    }

    public static String convertDateToDisplayDate(Date date){
        try {
            DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
            return dateFormat.format(date);
        } catch (Exception err) {
            return null;
        }
    }

    public static String convertDateToStringDate(Date date){
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            return dateFormat.format(date);
        } catch (Exception err) {
            return null;
        }
    }

    public static String getTimeForError(Date date){
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MMM-dd-yyyy   h:mma");
            return simpleDateFormat.format(date);

        } catch (Exception err) {
            return null;
        }
    }

    public static Date convertStringToDateForError(String strDate){
        try {
            // used for server
            DateFormat originalFormat = new SimpleDateFormat("MMM-dd-yyyy   h:mma");
            return originalFormat.parse(strDate);
        } catch (Exception err) {
            return null;
        }
    }

    public static Date convertStringToDate(String strDate){
        try {
            // used for server
            DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd");
            return originalFormat.parse(strDate);
        } catch (Exception err) {
            return null;
        }
    }

    public static Date convertStringToDate2(String strDate){
        try {
            DateFormat originalFormat = new SimpleDateFormat("MM-dd-yyyy");
            return originalFormat.parse(strDate);
        } catch (Exception err) {
            return null;
        }
    }

    public static Date addDaysToDate(Date date, int daysCount)
    {
        final Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, daysCount);
        return c.getTime();
    }

    public static String convertToMonthName(float value) {
        try {
            String monthFormat = "";

            if (value == 1) {
                monthFormat = "January";
            } else if (value == 2) {
                monthFormat = "February";
            } else if (value == 3) {
                monthFormat = "March";
            } else if (value == 4) {
                monthFormat = "April";
            } else if (value == 5) {
                monthFormat = "May";
            } else if (value == 6) {
                monthFormat = "June";
            } else if (value == 7) {
                monthFormat = "July";
            } else if (value == 8) {
                monthFormat = "August";
            } else if (value == 9) {
                monthFormat = "September";
            } else if (value == 10) {
                monthFormat = "October";
            } else if (value == 11) {
                monthFormat = "November";
            } else if (value == 12) {
                monthFormat = "December";
            }

            return monthFormat;
        } catch (Exception err) {
            return null;
        }
    }

    public static String getIntMonth(Date date) {
        try {
            DateFormat format = new SimpleDateFormat("MM");
            return format.format(date);
        } catch (Exception err) {
            return null;
        }
    }

    public static String getYear(Date date) {
        try {
            DateFormat format = new SimpleDateFormat("yyyy");
            return format.format(date);
        } catch (Exception err) {
            return null;
        }
    }
}
