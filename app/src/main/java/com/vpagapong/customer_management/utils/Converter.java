package com.vpagapong.customer_management.utils;

import android.database.Cursor;

import java.text.DecimalFormat;

public class Converter {

    public static double ObjectToDouble(Cursor cursor, String fieldName) {
        try {
            String sValue = cursor.getString(cursor.getColumnIndex(fieldName));
            if (sValue == null || sValue.isEmpty()) return 0;
            return Double.parseDouble(sValue);
        } catch (NumberFormatException err) {
            return 0;
        }
    }

    public static int ObjectToInt(Cursor cursor, String fieldName) {
        try {
            String sValue = cursor.getString(cursor.getColumnIndex(fieldName));
            if (sValue == null || sValue.isEmpty()) return 0;
            return Integer.parseInt(sValue);
        } catch (NumberFormatException err) {
            return 0;
        }
    }

    public static String ObjectToString(Cursor cursor, String fieldName) {
        try {
            return cursor.getString(cursor.getColumnIndex(fieldName));
        } catch (Exception err) {
            return "";
        }
    }

//    public static Date ObjectToDate(Cursor cursor, String fieldName)
//    {
//        try
//        {
//            return DateTimeHandler.convertStringtoDate(ObjectToString(cursor,fieldName));
//        } catch (Exception err)
//        {
//            return null;
//        }
//    }

    public static boolean ObjectToBoolean(Cursor cursor, String fieldName) {
        try {
            int result = ObjectToInt(cursor, fieldName);
            return result == 1;
        } catch (Exception err) {
            return false;
        }
    }

    public static int BooleanToInt(boolean boolValue) {
        try {
            return boolValue ? 1 : 0;
        } catch (Exception err) {
            return 0;
        }
    }

    public static boolean IntToBoolean(int intValue) {
        return intValue == 1;
    }

    public static String ConvertCurrencyDisplay(double amount) {
        DecimalFormat formatter = new DecimalFormat("#,###.00");
        return formatter.format(amount);
    }

}
